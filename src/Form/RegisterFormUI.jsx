import React from 'react';
import PropTypes from 'prop-types';
import './RegisterForm.css';

const RegisterFormUI = (props) => {
  const {
    values,
    errors,
    touched,
    handleInputChange,
    handleSubmit,
  } = props;

  const isError = (field) => errors[field] && touched[field];

  return (
    <div className="RegisterForm">
      <h1>Registration form</h1>
      <form onSubmit={handleSubmit}>
        {/* Email */}
        <div className={`form-item${isError('email') ? ' has-error' : ''}`}>
          <label htmlFor="email">Email</label>
          <input
            type="text"
            id="email"
            value={values.email}
            onChange={(e) => handleInputChange(e, 'email')}
          />
          {isError('email') && (
            <span className="error-message">
              {errors.email.map((err, k) => (
                <span key={k} className="error-item">
                  {err}
                </span>
              ))}
            </span>
          )}
        </div>
        {/* Password */}
        <div className={`form-item${isError('password') ? ' has-error' : ''}`}>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={values.password}
            onChange={(e) => handleInputChange(e, 'password')}
          />
          {isError('password') && (
            <span className="error-message">
              {errors.password.map((err, k) => (
                <span key={k} className="error-item">
                  {err}
                </span>
              ))}
            </span>
          )}
        </div>
        {/* Confirm */}
        <div className={`form-item${isError('confirm') ? ' has-error' : ''}`}>
          <label htmlFor="confirm">Confirm</label>
          <input
            type="password"
            id="confirm"
            value={values.confirm}
            onChange={(e) => handleInputChange(e, 'confirm')}
          />
          {isError('confirm') && (
            <span className="error-message">
              {errors.confirm.map((err, k) => (
                <span key={k} className="error-item">
                  {err}
                </span>
              ))}
            </span>
          )}
        </div>
        {/* Submit Button */}
        <div className="form-item">
          <input type="submit" value="Register!" />
        </div>
      </form>
    </div>
  );
};

RegisterFormUI.propTypes = {
  values: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default RegisterFormUI;