import React, { Component } from 'react';
import RegisterFormUI from './RegisterFormUI';
import './RegisterForm.css';

class RegistrationForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      touched: {},
      errors: {},
      values: {
        email: '',
        password: '',
        confirm: '',
      },
    };
  }

  handleInputChange = (e, field) => {
    const errors = { ...this.state.errors };
    delete errors[field];

    this.setState((prevState) => ({
      ...prevState,
      touched: {
        ...prevState.touched,
        [field]: true,
      },
      values: {
        ...prevState.values,
        [field]: e.currentTarget.value,
      },
      errors,
    }));
  };

  validateForm = (values) => {
    let errors = {};

    // Validation logic here...

    this.setState({
      ...this.state,
      errors,
    });

    return Object.keys(errors).length === 0;
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (!this.validateForm(this.state.values)) {
      return;
    }

    alert(JSON.stringify(this.state.values));
  };

  render() {
    const { values, errors, touched } = this.state;

    return (
      <RegisterFormUI
        values={values}
        errors={errors}
        touched={touched}
        handleInputChange={this.handleInputChange}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

export default RegistrationForm;