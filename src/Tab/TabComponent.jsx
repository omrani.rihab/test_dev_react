import React from 'react';
import RegisterForm from './Form/RegisterForm'; // Import the refactored RegisterForm component
import './Tab/Tab.css';

const LOGIN = 'login';
const REGISTER = 'register';

const TabContainer = ({ children }) => (
  <div className="tab-container">
    {children}
  </div>
);

const TabHeader = ({ filter, children }) => (
  <div className={`tab-header ${filter}`}>
    {children}
  </div>
);

const TabContent = ({ filter, children }) => (
  <div className={`content ${filter === LOGIN ? 'login' : 'register'}`}>
    {children}
  </div>
);

const TabComponent = () => (
  <TabContainer>
    <TabHeader filter={LOGIN}>J'ai un compte</TabHeader>
    <TabHeader filter={REGISTER}>Je n'ai pas de compte</TabHeader>

    <TabContent filter={LOGIN}>
      {/* Assume Login component is implemented */}
      {/* <Login /> */}
    </TabContent>
    <TabContent filter={REGISTER}>
      <RegisterForm onSubmit={(values) => console.log(values)} />
    </TabContent>
  </TabContainer>
);

export default TabComponent;
